#pragma once

#include <iostream>

#include <srrg_image_utils/depth_utils.h>
#include <srrg_l2c_types/dataset.h>
#include "frame_handler.h"
#include "ros_utils.h"
 
#include <rosbag/bag.h>
#include <sensor_msgs/JointState.h>
#include <nav_msgs/Odometry.h>
#include <tf2_msgs/TFMessage.h>
#include <sensor_msgs/Image.h>
#include <rosbag/view.h>
#include <cv_bridge/cv_bridge.h>

namespace srrg_l2c {

  class BagLoader {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    BagLoader();
    BagLoader(const std::string& bag_file);
    ~BagLoader();

    void setBagFile(const std::string& bag_file_);
    void setJointTopic(const std::string& joint_topic_,
                       const int joint_number_ = 2);
    void setOdomTopic(const std::string& odom_topic_);
    void setImageTopic(const std::string& sensor_name,
                       const std::string& depth_topic,
                       const std::string& rgb_topic = "");
    void setFramesHandlerVector(const FramesHandlerVector& frames_handler);

    void load(Dataset& dataset_,
              SensorDatasetMap& sensor_dataset_);

  protected:
    
    void generateJointMessage(Dataset& dataset_,
                              const sensor_msgs::JointStateConstPtr& joint_message_,
                              const double& timestamp_);
    void generateOdomMessage(Dataset& dataset_,
                             const nav_msgs::OdometryConstPtr& odom_message_,
                             const double& timestamp_);
    void generateTfMessage(SensorDatasetMap& sensor_dataset_,
                           const tf2_msgs::TFMessageConstPtr& tf_message_,
                           const double& timestamp_);
    void generateImageMessage(SensorDatasetMap& sensor_dataset_,
                              const sensor_msgs::ImageConstPtr& image_message_,
                              const double& timestamp_);

    
  private:
    std::string _bag_file;
    std::string _joint_topic;
    int _joint_number;
    std::string _odom_topic;
    std::pair<std::string, std::string> _image_topics;
    FramesHandlerVector _frames_handler_vector;
  };
  
}
