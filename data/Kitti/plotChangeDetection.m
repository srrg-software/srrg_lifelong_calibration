close all
clear
clc

%arg_list = argv ();
%filename = arg_list{1}
filename = "output_change_detection.txt"
data = load("-ascii",filename)

x_axis = 1:1:size(data,1)

determinant = data(:,1);
eig_ratio = data(:,2);
product = data(:,3);

linewidth = 2;

h = figure(1);

%set(h, 'paperunits', 'centimeters');
%PS = findall(h,'-property','papersize');
%set(PS,'papersize',[3 2.5]);
%PP = findall(h,'-property','paperposition');
%set(PP,'paperposition', [0,0 3 2.5]);
semilogy(x_axis, product, '-r', "linewidth", linewidth);
hold on;
semilogy(x_axis, product, '-o', "linewidth", linewidth);
grid;
FN = findall(h,'-property','FontName');
set(FN,'FontName','/usr/share/fonts/dejavu/DejaVuSerifCondensed.ttf');
FS = findall(h,'-property','FontSize');
set(FS,'FontSize',16);
%set(h, 'PaperPosition', [0 0 1 15]); %x_width=10cm y_width=15cm
