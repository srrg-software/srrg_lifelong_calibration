close all
clear
clc

%arg_list = argv ();
%filename = arg_list{1}
%samples = str2num(arg_list{2})
filename = "10/10_imu_gt.txt";
samples = 200;
dataset = load(filename);
dataset_size = size(dataset,1)

X = 4;
Y = 8;
Z = 12;

linewidth = 2;
point_linewidth = 6;
color = 'b';
points_style = '*g';

plot3(dataset(:,X), dataset(:,Y), dataset(:,Z), "linewidth", linewidth, color);
grid;
axis equal;
hold on;

for i=1:samples:dataset_size
  data = dataset(i,:);
  pos = [data(X), data(Y), data(Z)]
  hold on;
  plot3(data(X), data(Y), data(Z), points_style, "linewidth", point_linewidth);
endfor


pause
