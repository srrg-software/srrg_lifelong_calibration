#################################
#      TEST TIME ESTIMATE       #
#################################
add_executable(test_time_estimate
  test_time_estimate.cpp
)
target_link_libraries(test_time_estimate
  srrg_l2c_types_library
  srrg_l2c_kinematics_library
  srrg_l2c_solvers_library
  ${catkin_LIBRARIES}
)

#################################
#    TEST CROSS LINEARIZER      #
#################################
add_executable(test_cross_linearizer
  test_cross_linearizer.cpp
)
target_link_libraries(test_cross_linearizer
  srrg_l2c_types_library
  srrg_l2c_kinematics_library
  srrg_l2c_solvers_library
  ${catkin_LIBRARIES}
)

#################################
# TEST DIFFERENTIAL DRIVE KIN   #
#################################
add_executable(test_differential_drive_kinematics
  test_differential_drive_kinematics.cpp
)
target_link_libraries(test_differential_drive_kinematics
  srrg_l2c_types_library
  srrg_l2c_utils_library
  srrg_l2c_kinematics_library
  srrg_l2c_solvers_library
  ${catkin_LIBRARIES}
)

#################################
#  TEST FORKLIFT KINEMATICS     #
#################################
add_executable(test_forklift_kinematics
  test_forklift_kinematics.cpp
)
target_link_libraries(test_forklift_kinematics
  srrg_l2c_types_library
  srrg_l2c_utils_library
  srrg_l2c_kinematics_library
  srrg_l2c_solvers_library
  ${catkin_LIBRARIES}
)

#################################
#    TEST YOUBOT KINEMATICS     #
#################################
add_executable(test_youbot_kinematics
  test_youbot_kinematics.cpp
)
target_link_libraries(test_youbot_kinematics
  srrg_l2c_types_library
  srrg_l2c_utils_library
  srrg_l2c_kinematics_library
  srrg_l2c_solvers_library
  ${catkin_LIBRARIES}
)
