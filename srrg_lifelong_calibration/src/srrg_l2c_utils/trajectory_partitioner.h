#pragma once
#include "trajectory_analyzer.h"
#include "utils.h"

namespace srrg_l2c {

  struct TrajectoryPortion{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    Measure::Status status = Measure::Status::Unclassified;
    DatasetPtr dataset = nullptr;
    SensorDatasetMap sensor_dataset;
    double start_time = 0.0;
    double duration = 0.0;
    int number_of_sample = 0;
    bool used = false;
    void reset() {
      status = Measure::Status::Unclassified;
      dataset = nullptr;
      sensor_dataset.clear();
      start_time = 0.0;
      duration = 0.0;
      number_of_sample = 0;
      used = false;
    } 
  };

  typedef std::vector<TrajectoryPortion,
    Eigen::aligned_allocator<TrajectoryPortion> > TrajectoryPortionVector;

  class TrajectoryPartitioner {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    TrajectoryPartitioner();
    ~TrajectoryPartitioner();

    struct Config{
      // minimum time duration for each portion
      double min_duration = 0.5;
      double rate = 10; // Hz
      int min_samples = 100;
    };

    struct Stats {
      int number_of_portions = 0;
      double total_duration = 0;
      int number_of_straight_constant = 0;
      double straight_constant_duration = 0.0;
      int number_of_straight_variable = 0;
      double straight_variable_duration = 0.0;
      int number_of_rotate_constant = 0;
      double rotate_constant_duration = 0.0;
      int number_of_rotate_variable = 0;
      double rotate_variable_duration = 0.0;
      int number_of_arc_constant = 0;
      double arc_constant_duration = 0.0;
      int number_of_arc_variable = 0;
      double arc_variable_duration = 0.0;
      int number_of_still = 0;
      double still_duration = 0.0;
      const void print() const;
      void reset();
    };
    
    const Config& config() const {return _config;}
    Config& mutableConfig() {return _config;}
    const Stats& stats() const {return _stats;}
    
    // to access the trajectory analyzer config
    TrajectoryAnalyzer& trajectoryAnalyzer() {return _trajectory_analyzer;}
    
    void compute(TrajectoryPortionVector& trajectories_,
                 Dataset& dataset_,
                 const SensorDatasetMap& sensor_dataset_);

    void computeNormalized(TrajectoryPortionVector& trajectories_,
                           Dataset& dataset_,
                           const SensorDatasetMap& sensor_dataset_);

  protected:
    void updateStats(const TrajectoryPortion& portion_);
    void getSensorPortion(TrajectoryPortion& portion_,
                          const SensorDatasetMap& sensor_dataset_);
    void getDatasetPortion(TrajectoryPortion& portion_,
                           const Dataset& dataset_);
    
  private:
    Config _config;
    Stats _stats;
    TrajectoryAnalyzer _trajectory_analyzer;
    
  };
  
}


