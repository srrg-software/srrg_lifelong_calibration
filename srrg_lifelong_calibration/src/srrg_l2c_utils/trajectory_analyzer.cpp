#include "trajectory_analyzer.h"

namespace srrg_l2c {

  TrajectoryAnalyzer::TrajectoryAnalyzer() {    
  }

  TrajectoryAnalyzer::~TrajectoryAnalyzer() {    
  }

  void TrajectoryAnalyzer::getCurrentStatus(Measure::Status& status_,
                                            const real& linear_vel_norm_,
                                            const real& angular_vel_norm_,
                                            const real& linear_acc_norm_,
                                            const real& angular_acc_norm_) {      

    if(linear_vel_norm_ < _config.translational_velocity_thresh && angular_vel_norm_ < _config.rotation_velocity_thresh)
      status_ = Measure::Status::Still;
    else if(linear_vel_norm_ > _config.translational_velocity_thresh && angular_vel_norm_ < _config.rotation_velocity_thresh) {
      if(linear_acc_norm_ < _config.translational_acceleration_thresh)
        status_ = Measure::Status::StraightConstant;
      else
        status_ = Measure::Status::StraightVariable;
    }
    else if(linear_vel_norm_ > _config.translational_velocity_thresh && angular_vel_norm_ > _config.rotation_velocity_thresh) {
      if(linear_acc_norm_ < _config.translational_acceleration_thresh && angular_acc_norm_ < _config.rotation_acceleration_thresh)
        status_ = Measure::Status::ArcConstant;
      else
        status_ = Measure::Status::ArcVariable;
    }
    else if(linear_vel_norm_ < _config.translational_velocity_thresh && angular_vel_norm_ > _config.rotation_velocity_thresh) {
      if(angular_acc_norm_ < _config.rotation_acceleration_thresh)
        status_ = Measure::Status::RotateConstant;
      else
        status_ = Measure::Status::RotateVariable;
    }
    
  }

  
  void TrajectoryAnalyzer::compute(Dataset& dataset_) {

    Dataset::iterator data_it = dataset_.begin();
    Isometry3 prev_iso = (*data_it).second->isometry();
    double prev_time = (*data_it).first;
    
    Dataset::iterator motion_begin = dataset_.begin();
    Dataset::iterator motion_velocity_begin = dataset_.begin();

    bool init_velocities = true;
    Vector3 prev_linear_velocity = Vector3::Zero();
    real prev_angular_velocity = 0.0;
    
    for(++data_it; data_it != dataset_.end(); ++data_it) {
      
      const Isometry3& curr_iso = (*data_it).second->isometry();
      const double& curr_time = (*data_it).first;
      
      const double relative_time = curr_time - prev_time;
      if(relative_time < _config.observation_time) {        
        continue;
      }
      
      const Isometry3 relative_iso = prev_iso.inverse() * curr_iso;
     
      // barbarian way to compute velocity
      const Vector3 linear_velocity = relative_iso.translation() / relative_time;
      const real linear_norm = linear_velocity.norm();
      real angular_norm = computeAngle(relative_iso.linear());
      angular_norm /= relative_time;

      if(init_velocities) {
        prev_linear_velocity = linear_velocity;
        prev_angular_velocity = angular_norm;
        init_velocities = false;
      }

      // barbarian way to compute acceleration
      const Vector3 linear_acceleration = (linear_velocity - prev_linear_velocity) / relative_time;
      const real linear_acc_norm = linear_acceleration.norm();
      const real angular_acc_norm = (angular_norm - prev_angular_velocity) / relative_time;

      //      std::cerr << FRED("LIN ACC: ") << linear_acc_norm << FRED("|  ANG ACC: ") << angular_norm << std::endl;
      
      Measure::Status current_status = Measure::Status::Unclassified;
      getCurrentStatus(current_status,
                       linear_norm,
                       angular_norm,
                       linear_acc_norm,
                       angular_acc_norm);

      for(; motion_begin != data_it; ++motion_begin)
        (*motion_begin).second->status() = current_status;

      motion_begin = data_it;
      prev_iso = curr_iso;
      prev_time = curr_time;      
      prev_linear_velocity = linear_velocity;
      prev_angular_velocity = angular_norm;
    }
  }
  
}
