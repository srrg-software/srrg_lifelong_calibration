#ifndef _L2C_MEASURE_H_
#define _L2C_MEASURE_H_

#include <srrg_kdtree/kd_tree.hpp>

#include "defs.h"
#include <memory>
#include <map>
#include <fstream>
#include <iostream>
#include <iomanip>

#include <srrg_l2c_utils/utils.h>

#define POINT_SIZE 3

namespace srrg_l2c {

  using FloatImage = srrg_core::FloatImage;
  
  class Measure;
  /** shared pointer of a Measure */
  typedef std::shared_ptr<Measure> MeasurePtr;

  /** KDTree for storing points of size POINT_SIZE and type real */
  typedef srrg_core::KDTree<real, POINT_SIZE> KDTree;
  /** shared pointer of a KDTree */
  typedef std::shared_ptr<KDTree> KDTreePtr;
  
  /*!
   * A Measure constitutes a sample of a dataset. It can be an encoder measurement
   * or an isometry measure. It can be specified if the measure is relative or absolute.
   * In case of cross-correlated sensors, a measure can be filled with points to perform
   * ICP-like registration.
   */
  class Measure {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    /** Measure type can be either Absolute or Relative */
    enum Type{Absolute = 0x0,
              Relative = 0x1};
    
    /** Motion Taxonomy {Every motion has its constant/variable speed version} */
    enum Status{StraightConstant     = 0x0,
                StraightVariable     = 0x1,
                RotateConstant       = 0x2,
                RotateVariable       = 0x3,
                ArcConstant          = 0x5,
                ArcVariable          = 0x6,
                Still                = 0x7,
                Unclassified         = 0x8};
    
    /** Measure c'tor with isometry measure
     * @param isometry_measure the measure value as isometry
     * @param type measure type {Absolute / Relative}
     */
    Measure(const Isometry3& isometry_measure, const Type& type = Absolute);

    /** Measure c'tor with encoder count
     * @param encoder_ticks the measure value as vector
     * @param type measure type {Absolute / Relative}
     */
    Measure(const VectorX& encoder_ticks, const Type& type = Absolute);

    /** sets depth image (float image with values in meters) */           
    void setDepthImage(const FloatImage& depth_image);
    
    /** sets rgb image */           
    void setRgbImage(const cv::Mat& rgb_image);
    
    /** computes relative measure given a reference */           
    Measure inFrame(const MeasurePtr& reference);

    /** initializes the KD-tree with a leaf range parameter */           
    void computeTree(const real& leaf_range = 0.9);

    /** transforms in place the current measure */           
    void transformInPlace(const Isometry3& T);
    
    /** gets encoder ticks */           
    const VectorX& encoderTicks() const;

    /** gets isometry measure */           
    const Isometry3& isometry() const;

    /** gets the KD-Tree points */           
    KDTree::VectorTDVector& points();

    /** gets the KD-Tree points */           
    const KDTree::VectorTDVector& points() const;

    /** gets has_tick attribute value */           
    const bool hasTicks() const;

    /** gets status attribute value */           
    Measure::Status& status();

    /** gets status attribute value */           
    const Measure::Status& status() const;

    /** gets type attribute value */           
    const Measure::Type& type() const;
    
    /** gets KD-tree */           
    const KDTree& kdTree() const;

    /** gets Depth-Image */           
    const FloatImage& depthImage() const;

    /** gets RGB-Image */           
    const cv::Mat& rgbImage() const;
    
    /** prints the measure info */
    const void print() const;

    const bool isGood() const;
    
  private:

    /** private empty c'tor */
    Measure();
    
    bool has_ticks_;                 // true if this is an encoder measurement
    VectorX encoder_ticks_;          // vector storing encoder ticks
    Isometry3 isometry_;             // Isometry storing isometry measurement
    KDTree::VectorTDVector points_;  // tree points
    KDTreePtr kd_tree_ptr_;          // the kd-tree
    Status status_;                  // Measurement status
    Type type_;                      // measurement type
    FloatImage depth_image_;         // depth image measure (if any)
    cv::Mat rgb_image_;              // rgb image measure (if any)
    bool has_depth_;                 // true if measurement store a depth img
    bool has_rgb_;                   // true if measurement store a rgb img
    
  };

  /** a Sample is defined as a pair double(timestamp) and Measure Ptr */
  typedef std::pair<double, MeasurePtr> Sample;
   
}


#endif
