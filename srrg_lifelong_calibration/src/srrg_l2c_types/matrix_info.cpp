#include "matrix_info.h"

namespace srrg_l2c {

  MatrixInfo::MatrixInfo(const MatrixX& m_) {
    matrix = m_;
    computed = false;
    determinant = 0;
    rank = 0;
    eig_ratio = 0;
  }

  void MatrixInfo::standardize(MatrixX& matrix) {
    VectorX variances(matrix.rows());
    for(size_t i = 0; i < matrix.rows(); ++i)
      variances(i) = sqrt(matrix(i,i));
    for(size_t i = 0; i < matrix.rows(); ++i)
      for(size_t j = 0; j < matrix.cols(); ++j)
        matrix(i,j) /= (variances(i)*variances(j));
  }  
  
  void MatrixInfo::standardize() {
    standardize(matrix);
  }
  
  void MatrixInfo::standardizeInverse() {
    MatrixX inverse = matrix.inverse();
    standardize(inverse);
    matrix = inverse.inverse();
  }

  
  void MatrixInfo::compute() {
    eig.compute(matrix, true);
    eig_ratio = eig.eigenvalues().real().minCoeff() / eig.eigenvalues().real().maxCoeff();
    Eigen::FullPivLU<MatrixX> lu_decomp(matrix);
    rank = lu_decomp.rank();
    determinant = matrix.determinant();
    computed = true;    
  }

  const void MatrixInfo::print() const {
    if(!computed) {
      std::cerr << FYEL("[MatrixInfo][print]: first compute()!") << std::endl;
      return;
    }
      
    const int rows = matrix.rows();
    const int cols = matrix.cols();

    const VectorXc& eigenvalues = eig.eigenvalues();
    const MatrixXc& eigenvectors = eig.eigenvectors();
    
    std::cerr << FYEL(BOLD("************ MatrixInfo *********")) << std::endl;
    std::cerr << FYEL(BOLD("Eigenvectors in column with corresponding eigenvalues below")) << std::endl;
    std::cerr << FYEL(" EigVec") << std::endl;
    for(size_t r = 0; r < rows; ++r) {
      for(size_t c = 0; c < cols; ++c) {
        std::cerr << std::setw(15) << eigenvectors(r,c).real() << " ";           
      }
      std::cerr << std::endl;
    }
    std::cerr << FYEL(" EigVal") << std::endl;
    for(size_t c = 0; c < cols; ++c)
      std::cerr << std::setw(15) << eigenvalues(c).real() << " ";
    std::cerr << std::endl;            
  }

  const void MatrixInfo::write(const std::string& filename) const {
    //todo
  }
  
}
