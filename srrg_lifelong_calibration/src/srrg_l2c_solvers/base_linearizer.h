#ifndef _L2C_BASE_LINEARIZER_H_
#define _L2C_BASE_LINEARIZER_H_

#include <srrg_l2c_types/sensor.h>
#include <srrg_l2c_types/dataset.h>
#include <srrg_l2c_kinematics/base_kinematics.h>
#include <srrg_l2c_utils/utils.h>

namespace srrg_l2c {

  /*! 
   * Base Class of Linearizers. Abstract class. <br>
   * This object is used to linearize a nonlinear
   * problems, providing as output the linear system:
   *
   * - `Hx = b`
   */  
  class BaseLinearizer {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
       
    /** empty c'tor */
    BaseLinearizer();

    /** empty d'tor */
    ~BaseLinearizer() {}

    /** sets the pointer to base kinematics
     * @param kinematics pointer to base kinematics.
     */
    void setKinematics(const BaseKinematicsPtr& kinematics);

    /** sets the epsilon for computing numeric jacobian
     * @param epsilon 
     */
    void setEpsilon(const real& epsilon);

    /** sets the epsilon for computing numeric jacobian on the time delay part
     * @param epsilon_time
     */
    void setEpsilonTime(const real& epsilon_time);

    /** sets the robust kernel threshold
     * @param kernel_threshold 
     */
    void setKernelThreshold(const real& kernel_threshold);

    /** sets the linearizer verbosity. If true, the linearizer will print out its stats
     * @param verbose
     */     
    void setVerbosity(const bool verbose);   

    /** sets pointer to sensor
     * @param sensor_ptr
     */     
    void setSensor(const SensorPtr& sensor_ptr);

    /** sets pointer to reference sensor (used when linearizing cross-constraints)
     * @param reference_sensor_ptr
     */     
    void setReferenceSensor(const SensorPtr& reference_sensor_ptr);

    /** gets pointer to sensor */
    SensorPtr sensor() {return sensor_ptr_;}

    /** gets pointer to reference sensor (used when linearizing cross-constraints) */
    SensorPtr referenceSensor() {return reference_sensor_ptr_;}

    /** sets reference and sensor dataset.
     * @param reference_dataset_ptr either odometry dataset or encoder dataset
     * @param sensor_dataset_ptr sensor dataset
     */     
    void setDatasets(const DatasetPtr& reference_dataset_ptr,
                     const DatasetPtr& sensor_dataset_ptr);

    /** set the sensor dataset */
    void setSensorDataset(const DatasetPtr& sensor_dataset_ptr);

    /** set the Reference sensor dataset (used when linearizing cross-constraints) */
    void setReferenceDataset(const DatasetPtr& reference_dataset_ptr);
    
    /** locks the time delay estimate, no matter what the sensor attribute value is */
    void lockTimeDelayEstimate(const bool lock);
    
    /** initializes the linearize. */
    virtual void init() = 0;    

    /** linearizes the nonlinear problem. */
    virtual void linearize(MatrixX& H,
                           VectorX& b,
                           real& chi,
                           int& inliers,
                           int& outliers) = 0;
    
  protected:
    
    /** computes error and Jacobian. */
    virtual bool errorAndJacobian(VectorX& e,
                                  MatrixX& J,     
                                  const MeasurePtr& relative_sensor_measure,
                                  const double& time,
                                  const double& period = 0.0);

    /** computes numeric Jacobian considering the time delay estimate. */
    virtual bool timeDelayJacobian(VectorX& J_col,
                                   const MeasurePtr& relative_sensor_measure,
                                   const double& period,
                                   const double& effective_time);


    BaseKinematicsPtr kinematics_ptr_;   // pointer to Kinematics (if specified)
    SensorPtr sensor_ptr_;               // pointer to Sensor
    SensorPtr reference_sensor_ptr_;     // pointer to Reference Sensor (is linearizing cross constraints)
    DatasetPtr sensor_dataset_ptr_;      // pointer to Sensor Dataset
    DatasetPtr reference_dataset_ptr_;   // pointer to Reference Dataset 
    Isometry3 backup_extrinsics_;        // backup of extrinsics parameters
    VectorX backup_odom_params_;         // backup of odom parameters (if any)
    real time_delay_backup_;             // backup of time delay parameter (if any)
    real transition_threshold_;          // threshold for splitting samples on transitions
    real rotation_threshold_;            // threshold for splitting samples on rotation
    real kernel_threshold_;              // robust kernel threshold
    real epsilon_;                       // epsilon for numeric jacobian
    real i_two_epsilon_;                 // 1. / 2*epsilon precomputed
    real epsilon_time_;                  // epsilon for numeric jacobian of time delay
    real i_two_epsilon_time_;            // 1. / 2*epsilon_time precomputed
    int odom_params_size_;               // size of odom parameter vector (if any)
    bool estimate_time_delay_;           // flag to estimate time delay
    bool time_delay_locked_;             // flag to lock time delay estimate
    int total_param_size_;               // total parameter size (odom + sensor)
    bool reference_dataset_relative_;    // true if the reference dataset is of type relative
    bool verbose_;                       // verbosity
    bool initialized_;                   // initialization flag
  };

  /** shared pointer of Linearizer */
  typedef std::shared_ptr<BaseLinearizer> BaseLinearizerPtr;

  /** const shared pointer of Linearizer */
  typedef std::shared_ptr<BaseLinearizer const> BaseLinearizerConstPtr;
  
}

#endif
