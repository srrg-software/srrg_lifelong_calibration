#include "base_linearizer.h"

namespace srrg_l2c {

  BaseLinearizer::BaseLinearizer() {
    kinematics_ptr_        = nullptr;
    sensor_ptr_            = nullptr;
    reference_sensor_ptr_  = nullptr;
    reference_dataset_ptr_ = nullptr;
    sensor_dataset_ptr_    = nullptr;
    odom_params_size_  = 0;
    estimate_time_delay_        = false;
    time_delay_locked_          = false;
    reference_dataset_relative_ = false;
    total_param_size_   = 0;
    verbose_            = false;
    initialized_        = false;
    epsilon_            = 1e-7;
    i_two_epsilon_      = .5 / epsilon_;      
    epsilon_time_       = 1e-3;
    i_two_epsilon_time_ = .5 / epsilon_time_;
    transition_threshold_ = .5;
    rotation_threshold_ = .6;
    kernel_threshold_   = 2.0;
    backup_extrinsics_.setIdentity();    
  }

  void BaseLinearizer::setKinematics(const BaseKinematicsPtr& kinematics_ptr) {
    if(!kinematics_ptr)
      throw std::runtime_error("[LinearizerSE2][setKinematics]: NULL Ptr to Kinematics");
    
    kinematics_ptr_ = kinematics_ptr;
    odom_params_size_ = (kinematics_ptr_->odomParams()).size();
  }
  
  void BaseLinearizer::setEpsilon(const real& epsilon) {
    epsilon_ = epsilon;
    i_two_epsilon_ = .5 / epsilon_;
  }

  void BaseLinearizer::setEpsilonTime(const real& epsilon_time) {
    epsilon_time_ = epsilon_time;
    i_two_epsilon_time_ = .5 / epsilon_time_;
  }
  
  void BaseLinearizer::setVerbosity(const bool verbose) {
    verbose_ = verbose;
  }
  
  void BaseLinearizer::setSensor(const SensorPtr& sensor_ptr) {
    sensor_ptr_ = sensor_ptr;
  }

  void BaseLinearizer::setDatasets(const DatasetPtr& reference_dataset_ptr,
                                   const DatasetPtr& sensor_dataset_ptr) {
    setReferenceDataset(reference_dataset_ptr);
    setSensorDataset(sensor_dataset_ptr);
  }
  
  void BaseLinearizer::setReferenceDataset(const DatasetPtr& reference_dataset_ptr) {
    reference_dataset_ptr_ = reference_dataset_ptr;
    if(reference_dataset_ptr_->type() == Measure::Type::Relative)
      reference_dataset_relative_ = true;
  }
  
  void BaseLinearizer::setSensorDataset(const DatasetPtr& sensor_dataset_ptr) {
    sensor_dataset_ptr_ = sensor_dataset_ptr;
  }
  
  void BaseLinearizer::setReferenceSensor(const SensorPtr& reference_sensor_ptr) {
    reference_sensor_ptr_ = reference_sensor_ptr;
  }
  
  void BaseLinearizer::setKernelThreshold(const real& kernel_threshold) {
    kernel_threshold_ = kernel_threshold;
  }
  
  void BaseLinearizer::lockTimeDelayEstimate(const bool lock) {
    time_delay_locked_ = lock;
  }
  
  bool BaseLinearizer::errorAndJacobian(VectorX& e,
                                        MatrixX& J,
                                        const MeasurePtr& relative_sensor_measure,
                                        const double& time,
                                        const double& period) {
    throw std::runtime_error("[BaseLinearizer][errorAndJacobian]: You should override this method to use it!");
  }
  
  bool BaseLinearizer::timeDelayJacobian(VectorX& J_col,
                                         const MeasurePtr& relative_sensor_measure,
                                         const double& period,
                                         const double& effective_time) {
    throw std::runtime_error("[BaseLinearizer][timeDelayJacobian]: You should override this method to use it!");
  }
      
}
