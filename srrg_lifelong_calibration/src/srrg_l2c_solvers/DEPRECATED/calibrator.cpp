#include "calibrator.h"

namespace srrg_l2c {

  Calibrator::Calibrator() {
    _kinematics = NULL;
    _initialized = false;
    _verbose = false;
    _iterations = 20;
    _epsilon = 1e-4;
    _epsilon = 1e-2;
  }

  Calibrator::~Calibrator() {
    std::cerr << BOLD(FRED("[Calibrator]: "));
    for(const TimeDelaySolverPair& id_solver : _solver_vector)
      delete id_solver.second;
    std::cerr << FRED(" destroyed!") << std::endl;
  }

  void Calibrator::setKinematics(BaseKinematics* kinematics_) {
    if(kinematics_ == NULL)
      throw std::runtime_error("[Calibrator][setKinematics]: NULL ptr");
    _kinematics = kinematics_;
    _odom_param_backup = _kinematics->odomParams();
    _odom_param_best = _kinematics->odomParams();
  }
    
  void Calibrator::setSensor(Sensor* sensor_) {
    if(sensor_ == NULL)
      throw std::runtime_error("[Calibrator][setSensor]: NULL ptr");
    BackupAndBest backup(sensor_->extrinsics());
    _sensor_vector.push_back(std::pair<BackupAndBest, Sensor*>(backup, sensor_));
    std::cerr << "Added sensor!" << std::endl;
  }
  
  void Calibrator::setEpsilon(const real& epsilon_) {
    _epsilon = epsilon_;
  }
  
  void Calibrator::setEpsilonTime(const real& epsilon_time_) {
    _epsilon_time = epsilon_time_;
  }
  
  void Calibrator::setVerbosity(const bool verbose_) {
    _verbose = verbose_;
  }
  
  void Calibrator::setIterations(const int iterations_) {
    _iterations = iterations_;
  }

  void Calibrator::computeTimeDelay(TimeDelay& time_delay_,
                                    const int id,
                                    const int num_samples,
                                    const int num_dim) {

    time_delay_._delays.resize(num_dim);
    int value = id;
    for(size_t i = 0; i < num_dim; ++i) {
      std::div_t quot_rem = std::div(value, num_samples);
      time_delay_._delays[i] = _config.min_delay + quot_rem.rem * _config.delay_sampling_time;
      value = quot_rem.quot;
    }
    
  }
  
  void Calibrator::init() {
    // compute number of sample per dimension
    const real delay_window =  _config.max_delay - _config.min_delay;
    const int number_of_sample = delay_window / _config.delay_sampling_time + 1;

    std::cerr << "initializing" << std::endl;
    
    // compute number of dimensions
    int num_dimensions = 0;
    for(const std::pair<BackupAndBest, Sensor*>& iso_sensor : _sensor_vector)
      if(iso_sensor.second->estimateTimeDelay())
        ++num_dimensions;    

    int total_number_of_solvers = number_of_sample;
    for(size_t i = 0; i < num_dimensions-1; ++i)
      total_number_of_solvers *= number_of_sample;   
    
    _solver_vector.resize(total_number_of_solvers);
    // init solvers set
    for(int i = 0; i < total_number_of_solvers; ++i) {
      Solver* solver = new Solver();
      solver->lockTimeDelayEstimate(true);
      for(const std::pair<BackupAndBest, Sensor*>& iso_sensor : _sensor_vector)
        solver->setSensor(iso_sensor.second);
      if(_kinematics)
        solver->setKinematics(_kinematics);
      solver->setEpsilon(_epsilon);
      solver->setEpsilonTime(_epsilon_time);
      solver->setVerbosity(_verbose);
      solver->setIterations(_iterations);
      solver->init();
      TimeDelay time_delay;
      computeTimeDelay(time_delay, i, number_of_sample, num_dimensions);
      // init time delays
      _solver_vector[i] = TimeDelaySolverPair(time_delay, solver);
    }
    
    std::cerr << "initialized" << std::endl;
    _initialized = true;
    
  }
  

  void Calibrator::resetBackupParams() {
    if(_kinematics)
      _kinematics->setOdomParams(_odom_param_backup);
    for(std::pair<BackupAndBest, Sensor*>& backup_sensor_pair : _sensor_vector){
      backup_sensor_pair.second->setExtrinsics(backup_sensor_pair.first._backup);
    } 
  }

  void Calibrator::storeBestParams() {
    if(_kinematics)
      _odom_param_best = _kinematics->odomParams();
    for(std::pair<BackupAndBest, Sensor*>& backup_sensor_pair : _sensor_vector){
      backup_sensor_pair.first._best = backup_sensor_pair.second->extrinsics();
    }
  }

  void Calibrator::setBestParams() {
    if(_kinematics)
      _kinematics->setOdomParams(_odom_param_best);
    for(std::pair<BackupAndBest, Sensor*>& backup_sensor_pair : _sensor_vector){
      backup_sensor_pair.second->setExtrinsics(backup_sensor_pair.first._best);
    }
  }
  
  void Calibrator::setTimeDelay(const TimeDelay& time_delay_) {
    int curr = 0;
    const std::vector<real>& delays = time_delay_._delays;
    for(std::pair<BackupAndBest, Sensor*>& backup_sensor_pair : _sensor_vector){
      if(backup_sensor_pair.second->estimateTimeDelay())
        backup_sensor_pair.second->setTimeDelay(delays[curr++]);
    }
  }
  
  void Calibrator::compute(const TimestampMeasurePtrMap& reference_dataset_,
                           const SensorDatasetMap& sensor_dataset_) {
    if(!_initialized)
      throw std::runtime_error("[Calibrator][compute]: call the init method first!");

    BestSolver best_solver;
    best_solver.delay_solver_pair.second = NULL;
    
    // for all solvers    
    for(size_t i = 0; i < _solver_vector.size(); ++i){
      const TimeDelay& time_delay = _solver_vector[i].first;
      Solver& solver = *(_solver_vector[i].second);
      
      // set backup params and timeDelay
      resetBackupParams();
      setTimeDelay(time_delay);
         
      // compute
      std::cerr << "\rSolver: [" << i << "/" << _solver_vector.size() << "]";
      solver.compute(reference_dataset_, sensor_dataset_);
      
      //store chi2
      const Solver::Stats& stats = solver.stats();
      const real& last_chi = stats.chi_evolution[_iterations-1];
      if(last_chi < best_solver.chi) {
        best_solver.chi = last_chi;
        best_solver.delay_solver_pair = _solver_vector[i];
        storeBestParams();
      }      
    }
    
    std::cerr << "\n1th step of Compute Done: " << std::endl;
    best_solver.delay_solver_pair.first.print();

    // Now we have a good delay guess and a good initial solution
    // set this, unlock delay estimate, and optimize
    setTimeDelay(best_solver.delay_solver_pair.first);
    setBestParams();
    Solver& lonely_solver = *(best_solver.delay_solver_pair.second);
    lonely_solver.lockTimeDelayEstimate(false);
    lonely_solver.setIterations(_iterations*10);
    lonely_solver.compute(reference_dataset_, sensor_dataset_);    
    lonely_solver.stats().print();
    
  }

  
} //> namespace srrg_l2c
