#include "linearizer.h"

namespace srrg_l2c {

  Linearizer::Linearizer() : BaseLinearizer() {}
  
  void Linearizer::init() {
    if(!sensor_ptr_)
      throw std::runtime_error("[Linearizer][init]: missing sensor!");
    total_param_size_ = SE3_DIM + odom_params_size_;
    if(sensor_ptr_->estimateTimeDelay()){
      estimate_time_delay_ = true;
      //bdc, add a row for the time delay estimate
      total_param_size_ += 1;
    }    
    initialized_ = true;
  }


  void Linearizer::linearize(MatrixX& H,
                             VectorX& b,
                             real& chi,
                             int& inliers,
                             int& outliers) {
    if(!initialized_)
      throw std::runtime_error("[Linearizer][linarize]: missing initialization!");
    H.setZero();
    b.setZero();
    real current_chi = 0;

    if(kinematics_ptr_){
      backup_odom_params_ = kinematics_ptr_->odomParams();      
    }
    backup_extrinsics_ = sensor_ptr_->extrinsics();
    
    VectorX e = VectorX::Zero(SE3_DIM);
    MatrixX J = MatrixX::Zero(SE3_DIM, total_param_size_);
    int analyzed_data = 0;
    
    // get isom of first measure
    Isometry3 iso_start = sensor_dataset_ptr_->begin()->second->isometry();
    double time_start = sensor_dataset_ptr_->begin()->first;

    for(const Sample& time_measure : *sensor_dataset_ptr_) {
      const Isometry3& iso_current = time_measure.second->isometry();
      Isometry3 iso_relative = iso_start.inverse() * iso_current;
      if(!checkDistance(iso_relative, transition_threshold_, rotation_threshold_)) {
        continue;
      }
      iso_start = iso_current;
      MeasurePtr relative_sensor_measure(new Measure(iso_relative));
    
      const double& time_current = time_measure.first;

      const double period = time_current-time_start;
      // if(period < 0.5) {
      //   ++outliers;
      //   continue;
      // }
      
      if(errorAndJacobian(e, J,
                          relative_sensor_measure,
                          time_start,
                          period)) {

        real current_chi_square = e.transpose() * sensor_ptr_->informationMatrix() * e;
        real scale = 1.0;
        if(current_chi_square > kernel_threshold_) {
          scale *= sqrt(kernel_threshold_ / current_chi_square);
          outliers++;
        } 
        H += J.transpose() * sensor_ptr_->informationMatrix() * scale * J;
        b += J.transpose() * sensor_ptr_->informationMatrix() * scale * e;     
        ++inliers;
        current_chi += current_chi_square * scale;
        ++analyzed_data;
      } else {
        ++outliers;
      }
      
      time_start = time_current;
    }
    
    if(! analyzed_data) {
      std::cerr << FRED("[Linearizer][linearize]: analyzed data = 0!") << std::endl;
      return;
    }
    
    chi += (current_chi/analyzed_data);

    Vector6 prior_e;
    Matrix6 prior_J;
    J.setZero();
    Matrix6 prior_Omega;
    for(size_t i = 0; i < (sensor_ptr_->priors()).size(); ++i) {
      sensor_ptr_->priors().at(i)->computeMatrices(prior_e, prior_J, prior_Omega);
      J.block(0,0,SE3_DIM, SE3_DIM) = prior_J;
      H += J.transpose() * prior_Omega * J;
      b += J.transpose() * prior_Omega * prior_e;
    }
    
  }

  
  void Linearizer::errorWithPerturbation(VectorX& e,
                                         const MeasurePtr& reference_measure_ptr,
                                         const MeasurePtr& sensor_measure_ptr,
                                         const int index,
                                         const real& perturbation) {
    Isometry3 odometry_iso, sensor_iso, sensor_motion;
    Isometry3 sensor_extrinsics = sensor_ptr_->extrinsics();
    VectorX perturbated_params = backup_odom_params_;
    if(odom_params_size_) {
      Vector3 robot_motion;
      robot_motion.setZero();
      if(index > SE3_DIM-1) { // perturbate odom_params
        perturbated_params(index - SE3_DIM) += perturbation;
      }
      kinematics_ptr_->directKinematics(robot_motion, reference_measure_ptr->encoderTicks(), perturbated_params);
      Vector6 robot_motion6(Vector6::Zero());
      robot_motion6.head(2) = robot_motion.head(2);
      AngleAxis lin_part(robot_motion(2), Vector3::UnitZ());
      Quaternion q(lin_part);
      robot_motion6.tail(3) = Vector3(q.x(), q.y(), q.z());        
      Isometry3 se3_odometry_iso = srrg_core::v2t(robot_motion6);
      isoResize(odometry_iso, se3_odometry_iso);
    } else {
      isoResize(odometry_iso, reference_measure_ptr->isometry());
    }
    if(index > -1 && index < SE3_DIM) {
      Vector6 v_perturbation;
      v_perturbation.setZero();
      v_perturbation(index) = perturbation;
      sensor_extrinsics = sensor_extrinsics * srrg_core::v2t(v_perturbation);
    }
    sensor_iso = sensor_extrinsics.inverse() * odometry_iso * sensor_extrinsics;
    isoResize(sensor_motion, sensor_measure_ptr->isometry());
    e = srrg_core::t2v(sensor_motion.inverse() * sensor_iso);
    
  }


  bool Linearizer::timeDelayJacobian(VectorX& J_col,
                                     const MeasurePtr& relative_sensor_measure,
                                     const double& period,
                                     const double& effective_time) {

    // Then work on Time
    const double time_plus = effective_time + epsilon_time_;

    MeasurePtr reference_start_time_plus = reference_dataset_ptr_->interpolateMeasure(time_plus);
    MeasurePtr reference_end_time_plus = reference_dataset_ptr_->interpolateMeasure(time_plus + period);
    if(reference_start_time_plus == nullptr ||
       reference_end_time_plus == nullptr)
      return false;
        
    VectorX e_plus = VectorX::Zero(SE3_DIM);
    VectorX e_minus = VectorX::Zero(SE3_DIM);
    MeasurePtr reference_relative_time_plus(new Measure(reference_end_time_plus->inFrame(reference_start_time_plus)));
    errorWithPerturbation(e_plus, reference_relative_time_plus, relative_sensor_measure, -1, false);
    const double time_minus = effective_time - epsilon_time_;

    MeasurePtr reference_start_time_minus = reference_dataset_ptr_->interpolateMeasure(time_minus);
    MeasurePtr reference_end_time_minus = reference_dataset_ptr_->interpolateMeasure(time_minus + period);
    if(reference_start_time_minus == nullptr ||
       reference_end_time_minus == nullptr)
      return false;

    MeasurePtr reference_relative_time_minus(new Measure(reference_end_time_minus->inFrame(reference_start_time_minus)));
    errorWithPerturbation(e_minus, reference_relative_time_minus, relative_sensor_measure, -1, false);   
    J_col = (e_plus - e_minus) * i_two_epsilon_time_;
    
    if(J_col.norm() < 1e-6) {
      std::cerr << J_col.transpose() <<  std::endl;
      return false;
    }
    
    return true;
  }
  
  bool Linearizer::errorAndJacobian(VectorX& e,
                                    MatrixX& J,        
                                    const MeasurePtr& relative_sensor_measure,
                                    const double& time,
                                    const double& period) {

   
    double effective_time = time;
    effective_time += sensor_ptr_->timeDelay();

    MeasurePtr reference_start = reference_dataset_ptr_->interpolateMeasure(effective_time);
    
    MeasurePtr reference_end = reference_dataset_ptr_->interpolateMeasure(effective_time + period);
    
    if(reference_start == nullptr ||
       reference_end == nullptr) {
      e.setZero();
      J.setZero();
      return false;
    }

    // std::cerr << "required at time: " << effective_time << std::endl;
    // reference_start->print();
    // std::cerr << "required at time: " << effective_time+period << std::endl;
    // reference_end->print();
    
    //    std::cerr << ".";
    MeasurePtr reference_relative = MeasurePtr(new Measure(reference_end->inFrame(reference_start)));
    // std::cerr << "relative " << std::endl;
    // reference_relative->print();
    // std::cerr << std::endl;
    
    //    reference_relative->print();
    
    if(!reference_relative->isGood()) {
      e.setZero();
      J.setZero();
      return false;
    }
    
    // std::cerr << KYEL << "time: " << effective_time << "\tperiod: " << period_ << std::endl;      
    // std::cerr << KGRN << "start: ";
    // reference_start->print();
    // std::cerr << "end: ";
    // reference_end->print();
    // std::cerr << "relative: ";
    // std::cerr << KRED;
    // reference_relative->print();
    // std::cerr << RST;

    // std::cerr << std::endl;;    
    errorWithPerturbation(e, reference_relative, relative_sensor_measure, -1, false);
    //    std::cerr << "e (no pert): " << e_.transpose() << std::endl;
    //    std::exit(1);

    
    VectorX e_plus = VectorX::Zero(SE3_DIM);
    VectorX e_minus = VectorX::Zero(SE3_DIM);
    // first work on DIM, i.e. perturbate the Sensor Components
    for(size_t i = 0; i < SE3_DIM; ++i) {
      errorWithPerturbation(e_plus, reference_relative, relative_sensor_measure, i, epsilon_);
      errorWithPerturbation(e_minus, reference_relative, relative_sensor_measure, i, -epsilon_);
      J.col(i) = (e_plus - e_minus) * i_two_epsilon_;
    }

    // Then work on Time Delay, if needed, unless it is locked
    int param_id = SE3_DIM;
    if(estimate_time_delay_ && !time_delay_locked_){
      VectorX jacobian_col = VectorX::Zero(SE3_DIM);
      if(timeDelayJacobian(jacobian_col,
                           relative_sensor_measure,
                           period, effective_time)) {
        J.col(param_id) = jacobian_col;
        ++param_id;
      } else {
        e.setZero();
        J.setZero();
        return false;
      }
    } else if(estimate_time_delay_)
      ++param_id;
    
    
    // Finally work on odometry, if any
    for(size_t i = param_id; i < total_param_size_; ++i) {
      errorWithPerturbation(e_plus, reference_relative, relative_sensor_measure, i, epsilon_);
      errorWithPerturbation(e_minus, reference_relative, relative_sensor_measure, i, -epsilon_);
      J.col(i) = (e_plus - e_minus) * i_two_epsilon_;
    }   
    return true;
  }
  
}
