#pragma once
#include "solver.h"

namespace srrg_l2c {

  class Calibrator {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    // Full:   whole dataset
    // Keivan: best K portions (ordered by determinant)
    // Bre:    best ratio of eigenvalues to weight the determinants
    enum Mode{Full=0x0, Keivan=0x1, Bre=0x2};
    
    Calibrator(const Mode mode_ = Mode::Full);
    ~Calibrator() {}

    

  private:
    Mode _mode;
    Solver _solver;

  };

}
