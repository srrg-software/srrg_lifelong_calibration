#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_utils/file_reader.h>
#include <srrg_l2c_solvers/solver.h>

using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "offline_app: offline (from files) calibration app",
  "file format: timestamp x y z qw qx qy qz",
  "",
  "usage: offline_app -odom odom.txt -estimate sensor_estimate.txt",
  "-odom       <string>   txt file containing odom measurements",
  "-estimate   <string>   txt file containing pose sensor estimate measures",
  "-guess      <Vector6>  initial guess in form [x y z qx qy qz]",
  "-time_delay <flag>     turn on time delay estimate.",
  "-h          <flag>     this help",
  0
};

int main(int argc, char** argv){

  // bdc, offline app. Takes files as input and performs calibration
  // In particular it works with an odometry and a sensor estimate
  
  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 0;
  }

  int c = 1;

  std::string odom_file   = "";
  std::string sensor_file = "";
  Vector6 initial_guess;
  initial_guess.setZero();
  bool estimate_time_delay = false;

  // bdc, parameter reading
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-odom")) {
      c++;
      odom_file = argv[c];
    } else if (!strcmp(argv[c], "-estimate")) {
      c++;
      sensor_file = argv[c];
    } else if (!strcmp(argv[c], "-time_delay")) {
      estimate_time_delay = true;
    } else if (!strcmp(argv[c], "-guess")) {
      c++;
      for(size_t i = 0; i < 6; ++i)
        initial_guess(i) = std::atof(argv[c++]);
    }
    c++;
  }
  
  std::string sensor_name = "sensor";
  DatasetPtr odometry_dataset = DatasetPtr(new Dataset());
  DatasetPtr sensor_dataset = DatasetPtr(new Dataset());

  FileReader filereader;
  filereader.setFile(odom_file);
  if(!filereader.compute(*odometry_dataset))
    throw std::runtime_error("[Main]: Error while processing odometry dataset");
  filereader.setFile(sensor_file);
  if(!filereader.compute(*sensor_dataset))
    throw std::runtime_error("[Main]: Error while processing sensor dataset");
    
  SensorDatasetMap sensor_dataset_map;
  sensor_dataset_map.insert(SensorDatasetPair(sensor_name, sensor_dataset));
 
  SensorPtr sensor1 = SensorPtr(new Sensor(sensor_name));
  sensor1->setExtrinsics(initial_guess);
  // bdc, if needed, it is possible to `lock` a parameter, as in this case
  // we're locking Z-coord at 3.5
  sensor1->addPrior(1.2, Sensor::Z);
  if(estimate_time_delay)
    sensor1->setEstimateTimeDelay(true);
  
  Solver solver;  
  solver.setSensor(sensor1);
  const int iterations = 20; 
  solver.setIterations(iterations);
  solver.setEpsilon(1e-4);
  solver.setEpsilonTime(6e-2);
  
  solver.init();
  
  solver.compute(odometry_dataset, sensor_dataset_map);
  solver.stats().print();
  
  sensor1->print();
  
  return 0;
}
