#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_utils/file_reader.h>
#include <srrg_l2c_solvers/solver.h>
#include <srrg_l2c_utils/trajectory_splitter.h>
#include <srrg_l2c_types/matrix_info.h>

using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "offline_app_kitti: offline app designed for kitti-like data",
  "",
  "usage: offline_app_kitti -odom odom.txt -estimate sensor_est.txt",
  "-odom       <string>    txt file containing odom measurements",
  "-estimate   <string>    txt file containing pose sensor estimate measures",
  "-gt         <string>    txt file containing the actual param",
  "-h          <flag>      this help",
  0
};

Isometry3 getTransform(const std::string& filename) {
  std::ifstream file;
  file.open(filename);
  if(!file.is_open()) {
    std::cerr << KRED << "unable to read file: " << filename
              << RST << std::endl;      
    std::exit(0);
  }
  Matrix3 R = Matrix3::Zero();
  Vector3 t = Vector3::Zero();
  file >> R(0,0) >> R(0,1) >> R(0,2) >> t(0)
       >> R(1,0) >> R(1,1) >> R(1,2) >> t(1)
       >> R(2,0) >> R(2,1) >> R(2,2) >> t(2);

  Isometry3 T;
  T.linear() = R;
  T.translation() = t;
  return T;
}

int main(int argc, char** argv){

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 0;
  }

  int c = 1;

  std::string odom_file   = "";
  std::vector<std::string> sensor_files;
  std::vector<std::string> actual_params;
  Vector6Vector initial_guesses;
  bool output_results = false;
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-odom")) {
      c++;
      odom_file = argv[c];
    } else if (!strcmp(argv[c], "-estimate")) {
      c++;
      sensor_files.push_back(argv[c]);
    } else if (!strcmp(argv[c], "-gt")) {
      c++;
      actual_params.push_back(argv[c]);
    } else if (!strcmp(argv[c], "-o")) {
      output_results = true;
    } else if (!strcmp(argv[c], "-guess")) {
      c++;
      Vector6 current_guess(Vector6::Zero());
      for(size_t i = 0; i < 6; ++i)
        current_guess(i) = std::atof(argv[c++]);
      initial_guesses.push_back(current_guess);
    }
    c++;
  }

  Vector6 good_guess;
  good_guess << 0.9, -0.2, 0.7, -0.5, 0.5,-0.5;
  
  const int number_of_sensor = sensor_files.size();
  std::cerr << FGRN("[Main] Calibrating ") << number_of_sensor
            << FGRN(" sensors") << std::endl;
  
  std::vector<std::string> sensor_names(number_of_sensor, "sensor_");
  for(int i = 0; i < number_of_sensor; ++i)
    sensor_names[i] += std::to_string(i);

  DatasetPtr odometry_dataset = DatasetPtr(new Dataset());
  SensorDatasetMap sensor_datasets;
  for(size_t i = 0; i < number_of_sensor; ++i)    
    sensor_datasets.insert(std::make_pair(sensor_names[i], DatasetPtr(new Dataset())));

  
  FileReader filereader;
  filereader.setFile(odom_file);
  if(!filereader.compute12(*odometry_dataset))
    throw std::runtime_error("[Main]: Error while processing odometry dataset");
  for(size_t i = 0; i < number_of_sensor; ++i) {
    filereader.setFile(sensor_files[i]);
    if(!filereader.compute12(*sensor_datasets.at(sensor_names[i])))
      throw std::runtime_error("[Main]: Error while processing sensor dataset");    
  }
  
  // Analyze the dataset
  TrajectoryAnalyzer trajectory_analyzer;
  trajectory_analyzer.compute(*odometry_dataset);
  
  std::vector<Sensor*> sensors;
  for(size_t i = 0; i < number_of_sensor; ++i) {
    sensors.push_back(new Sensor(sensor_names[i]));
    if(i < initial_guesses.size())
      sensors[i]->setExtrinsics(initial_guesses[i]);
    sensors[i]->addPrior(0.746412, Sensor::Z);
  }

  
  TrajectorySplitter trajectory_splitter(TrajectorySplitter::Mode::Motion);
  trajectory_splitter.setDataset(sensor_datasets);
  SensorDatasetMapVector dataset_map_vector;
  trajectory_splitter.compute(dataset_map_vector);
  std::cerr << FYEL("Dataset Map Vector size: ") << dataset_map_vector.size() << std::endl;
  
  
  std::vector<std::pair<MatrixInfo, SensorDatasetMap> > scored_dataset_vector;
  
  for(size_t i = 0; i < dataset_map_vector.size(); ++i) {

    for(size_t s = 0; s < number_of_sensor; ++s)
      sensors[s]->setExtrinsics(good_guess);
    
    Solver tmp_solver;
    for(size_t s = 0; s < number_of_sensor; ++s)
      tmp_solver.setSensor(sensors[s]);
    
    tmp_solver.setIterations(10);
    tmp_solver.setEpsilon(1e-4);
    
    tmp_solver.init();
    tmp_solver.compute(odometry_dataset, dataset_map_vector[i]);

    MatrixInfo matrix_info(tmp_solver.H());
    matrix_info.compute();
    scored_dataset_vector.push_back(std::make_pair(matrix_info, dataset_map_vector[i]));    
  }

  const int k = scored_dataset_vector.size();
  //  std::cerr << BOLD(FYEL("k: ")) << k << std::endl;

  for(size_t s = 0; s < number_of_sensor; ++s)    
    sensors[s]->setExtrinsics(good_guess);

  Solver solver;
  for(size_t s = 0; s < number_of_sensor; ++s)
    solver.setSensor(sensors[s]);
  solver.setIterations(10);
  solver.setEpsilon(1e-4);    
  solver.init();

  double best_w = -1.0;
  int selected = -1;
  bool stop = false;
  int portion_used = 0;
  std::vector<bool> used(scored_dataset_vector.size(),false);
  while(!stop) {
    for(size_t i = 0; i < scored_dataset_vector.size(); ++i) {
      if(used[i])
        continue;
      const MatrixX H_sum = solver.H() + scored_dataset_vector[i].first.matrix;
      MatrixInfo sum_info(H_sum);
      sum_info.compute();
      double weighted_determinant = sum_info.determinant * sum_info.eig_ratio;
      if(weighted_determinant > best_w) {
        best_w = weighted_determinant;
        selected = i;
      }      
    }
    if(selected == -1){
      if(portion_used >= k)
        stop = true;
      else {
        best_w = -1.0;
      }
    } else {
      solver.compute(odometry_dataset, scored_dataset_vector[selected].second);
      MatrixInfo H_info(solver.H());
      H_info.compute();
      best_w = H_info.determinant * H_info.eig_ratio;
      ++portion_used;
      used[selected] = true;
      selected = -1;
    }    
  }
  

  // int current = 0;
  // for(std::pair<real, SensorDatasetMap> current_data : scored_dataset_map) {
  //   solver.compute(odometry_dataset, current_data.second);
  //   ++current;
  //   if(current == k)
  //     break;
  // }


  for(size_t i = 0; i < number_of_sensor; ++i)
    sensors[i]->print();

  std::ofstream output_file;
  if(output_file)
    output_file.open("output_dellacorte.txt");
  
  for(size_t i = 0; i < actual_params.size(); ++i) {
    // compute estimation error
    const Isometry3 actual_extrinsics =  getTransform(actual_params[i]);
    const Isometry3 extrinsics_error = actual_extrinsics.inverse() * sensors[i]->extrinsics();
    std::cerr << KRED << sensor_names[i] << RST << std::endl;
    std::cerr << FRED("gt [extrinsics]: ") << srrg_core::t2v(actual_extrinsics).transpose() << std::endl;
    const double error_t = extrinsics_error.translation().norm();
    const double error_r = computeAngle(extrinsics_error.linear());
    std::cerr << FRED(" err_t: ") << error_t
              << FRED(" err_r: ") << error_r << std::endl;
    output_file << error_t << " " << error_r << std::endl;    
  }

  if(output_file)
    output_file.close();

  
  for(size_t i = 0; i < number_of_sensor; ++i)
    delete sensors[i];
  
  return 0;
}
