#pragma once

#include "base_kinematics.h"

namespace srrg_l2c {

  class ForkliftKinematics : public BaseKinematics {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    const static int intrinsics_size = 2;
    // w_Dx : i.e. steering fix wheel distance x-axis
    // w_Dy : i.e. steering fix wheel distance y-axis

    // input_data_ format:
    //
    // input_data_(0) = drive_dist, i.e. odom_drive_vel * time_diff
    // input_data_(1) = odom_alpha

    
    ForkliftKinematics() {
      odom_params_.setZero(intrinsics_size);
      // default values from oru
      odom_params_(0) = 0.68;
      odom_params_(1) = 0.0;
    }
    
    ~ForkliftKinematics(){
      std::cerr << FRED("[ForkliftKinematics] destroyed\n");
    }

    void directKinematics(Vector3& pose,
                          const VectorX& input_data);

    void directKinematics(Vector3& pose,
                          const VectorX& input_data,
                          const VectorX& odom_params);

    void update(const VectorX& dx_);

    const void print() const;
    
  };

  typedef std::shared_ptr<ForkliftKinematics> ForkliftKinematicsPtr;
  typedef std::shared_ptr<ForkliftKinematics const> ForkliftKinematicsConstPtr;

  
}
