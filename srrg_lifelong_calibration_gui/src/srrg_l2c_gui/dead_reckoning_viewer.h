#pragma once
#include <srrg_types/types_mat.hpp>
#include <srrg_types/cloud_3d.h>
#include <srrg_image_utils/point_image_utils.h>

#include <srrg_l2c_kinematics/base_kinematics.h>
#include <srrg_l2c_types/sensor.h>
#include <srrg_l2c_types/dataset.h>

#include <fstream>

#include <qapplication.h>
#include <qglviewer.h>
#include <QKeyEvent>

class QPaintEvent;
class QPainter;

#if QT_VERSION >= 0x050000
typedef qreal qglviewer_real;
#else
typedef float qglviewer_real;
#endif

namespace srrg_l2c {

  typedef std::vector<Eigen::Isometry3f,
    Eigen::aligned_allocator<Eigen::Isometry3f> > Isometry3Vector;
  typedef std::map<double, Eigen::Isometry3f> TimeIsometry3Vector;
  
  using Float3Image = srrg_core::Float3Image;
  
  class StandardCamera: public qglviewer::Camera {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  protected:
    virtual qglviewer_real zFar() const { return 3000; }
    virtual qglviewer_real zNear() const { return 1; }
  };

  class DeadReckoningViewer : public QGLViewer {
  public:
    DeadReckoningViewer() {
      cam =new StandardCamera();
      setCamera(cam);

      Vector6 v;
      v << 0.0f,0.0f,0.0f,-0.5f,0.5f,-0.5f;
      Isometry3 t = srrg_core::v2t(v);

      Vector3 pos (0,0,-2);
      Vector3 dir = t.rotation()*Vector3(0,-1,0);
      cam->setPosition(qglviewer::Vec(pos.x(),pos.y(),pos.z()));
      cam->setUpVector(qglviewer::Vec(0.0f, -1.0f, 0.0f));
      cam->lookAt(qglviewer::Vec(pos.x()+dir.x(),pos.y()+dir.y(),pos.z()+dir.z()));

      _dataset_ptr = nullptr;
      _sensor_dataset_ptr = nullptr;
      _robot_ptr = nullptr;

      _rotate.setIdentity();
      _rotate.linear() << 1, 0, 0, 0, 0, 1, 0, 1, 0;      
      _cylinder_radius = 0.5;
      _cylinder_height = 0.3;
      _increment = true;
    }
    void init() {
      QColor white = QColor(Qt::white);
      setBackgroundColor(white);
      glEnable(GL_BLEND);
      glEnable(GL_LIGHTING);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glDisable(GL_LIGHT0);
      const GLfloat pos[4] = {0.f, 0.f, 0.f, 1.0f};
      glLightfv(GL_LIGHT1, GL_POSITION, pos);
      glEnable(GL_LIGHT1);

      const GLfloat light_diffuse[4] = {1.0, 1.0, 1.0, 1.0};
      glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);     
    }

    void lookAt(const Isometry3& pose) {
      Vector6 v = srrg_core::t2v(pose);
      Vector3 dir = pose.rotation()*Vector3(0,-1,0);
            
      cam->setPosition(qglviewer::Vec(v(0),v(1),v(2)));
      cam->setUpVector(qglviewer::Vec(0.0f, -1.0f, 0.0f));
      cam->lookAt(qglviewer::Vec(v(0)+dir.x(),v(1)+dir.y(),v(2)+dir.z()));
      setCamera(cam);
    }

    void compute();
    
    virtual void draw();

    void setDataset(const DatasetPtr& dataset_) {
      _dataset_ptr = dataset_;
    }

    void setSensorDataset(const SensorDatasetMapPtr& sensor_dataset_) {
      _sensor_dataset_ptr = sensor_dataset_;
    }

    void setSensor(const SensorPtr& sensor_ptr) {
      _sensor_ptr = sensor_ptr;
      _direction_image.create(240, 320); //TODO clean this shit
      srrg_core::initializePinholeDirections(_direction_image,
                                             _sensor_ptr->cameraMatrix());
    }
    
    void setKinematics(const BaseKinematicsPtr& robot_) {
      _robot_ptr = robot_;
    }
    
  protected:

    virtual void  keyPressEvent(QKeyEvent *event);
    void colorFromStatus(const Measure::Status& status_);
    void drawTrajectory();
    void drawCloud();

    TimeIsometry3Vector _robot_poses;
    
    Isometry3 _rotate;
    DatasetPtr _dataset_ptr;
    SensorDatasetMapPtr _sensor_dataset_ptr;
    SensorPtr _sensor_ptr;
    BaseKinematicsPtr _robot_ptr;
    
    float _cylinder_radius;
    float _cylinder_height;
    bool _increment;

    StandardCamera* cam;

    srrg_core::Cloud3D _cumulative_cloud;
    
    Float3Image _direction_image;
  };

}
