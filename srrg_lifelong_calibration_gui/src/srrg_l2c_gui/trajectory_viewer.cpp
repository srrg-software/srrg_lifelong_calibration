#include "trajectory_viewer.h"
#include <qevent.h>

#include <srrg_gl_helpers/opengl_primitives.h>

namespace srrg_l2c {

  using namespace srrg_gl_helpers;
  
  void TrajectoryViewer::draw() {
    if(!_dataset)
      return;
    
    glPushMatrix();
    {
      //draw here the trajectory as set of reference frames
      for(const Sample& time_measure : *_dataset) {
        const MeasurePtr& measure = time_measure.second;
        glPushMatrix();
        {
          glMultMatrix(measure->isometry());
          glPushMatrix();
          {
            glScalef(0.1, 0.1, 0.1);
            drawReferenceSystem();
          }
          glPopMatrix();

          glPushMatrix();
          {
            glMultMatrix(_rotate);
            colorFromStatus(measure->status());
            drawCylinder(_cylinder_radius, _cylinder_height);
          }
          glPopMatrix();

        }
        glPopMatrix();
      }
    }
    glPopMatrix();
  }

  void TrajectoryViewer::colorFromStatus(const Measure::Status& status_) {
    if(status_ == Measure::Status::StraightConstant)
      glColor4f(0.5, 0.5, 1.0, 0.8);
    else if(status_ == Measure::Status::StraightVariable)
      glColor4f(0.8, 0.8, 1.0, 0.8);
    else if(status_ == Measure::Status::ArcConstant)
      glColor4f(0.5, 1.0, 0.5, 0.8);
    else if(status_ == Measure::Status::ArcVariable)
      glColor4f(0.8, 1.0, 0.8, 0.8);
    else if(status_ == Measure::Status::RotateConstant)
      glColor4f(1.0, 0.5, 0.5, 0.8);    
    else if(status_ == Measure::Status::RotateVariable)
      glColor4f(1.0, 0.8, 0.8, 0.8);    
    else
      glColor4f(0.1, 0.1, 0.1, 0.1);
  }
  
  void TrajectoryViewer::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
      
    case Qt::Key_Plus:
      _increment = true;
      break;
    case Qt::Key_Minus:
      _increment = false;
      break;
    case Qt::Key_R:
      if(_increment)
        _cylinder_radius += 0.1;
      else if(_cylinder_radius > 0.1)
        _cylinder_radius -= 0.1;
      break;
    case Qt::Key_H:
      if(_increment)
        _cylinder_height += 0.1;
      else if(_cylinder_height > 0.1)
        _cylinder_height -= 0.1;
      break;
    }
    updateGL();
  }
  
}
