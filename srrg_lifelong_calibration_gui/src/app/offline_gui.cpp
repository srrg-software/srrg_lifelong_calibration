#include <srrg_l2c_imgui/imgui/imgui.h>
#include <srrg_l2c_imgui/imgui/imgui_impl_glfw_gl3.h>
#include <stdio.h>
#include <srrg_l2c_imgui/imgui/gl3w.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_imgui/calibrator_imgui.h>

using namespace srrg_l2c;


static void error_callback(int error, const char* description)
{
  fprintf(stderr, "Error %d: %s\n", error, description);
}


const char* banner [] = {
  "offline_gui: from nw to valhalla",
  "",
  "usage: offline_gui -config configuration.yaml bagfile.bag",
  "-config  <string>        yaml configuration file, see default.yaml",
  "-h       <flag>          this help",
  0
};


int main(int argc, char** argv) {

  // Setup window
  glfwSetErrorCallback(error_callback);
  if (!glfwInit())
    return 1;
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
  GLFWwindow* window = glfwCreateWindow(1280, 720, "lifelong_calibrator_gui", NULL, NULL);

  glfwMakeContextCurrent(window);
  glfwSwapInterval(1); // Enable vsync
  gl3wInit();

  // Setup ImGui binding
  ImGui_ImplGlfwGL3_Init(window, true);

  bool show_test_window = false;
  ImVec4 clear_color = ImColor(114, 144, 154);


  
  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  int c = 1;
  std::string configuration_file = "";
  std::string bag_file = "";
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-config")) {
      c++;
      configuration_file = argv[c];
    } else {
      bag_file = argv[c];
    }
    c++;
  }

  if(configuration_file.empty())
    throw std::runtime_error("[LifelongCalibrator] specify a YAML configuration file");

  // TODO: remove ros_init dependency
  ros::init(argc, argv, "lifelong_calibration_offline_gui");
  ros::NodeHandle nh("~");
  ROS_INFO("node started");
  
  CalibratorGUI calibrator(CalibratorRos::OFFLINE);
  calibrator.setBagFile(bag_file);
  calibrator.initFromYaml(configuration_file);
  //  calibrator.compute();

  bool show_calibrator_GUI = true;

  while (!glfwWindowShouldClose(window)) {
    glfwPollEvents();
    ImGui_ImplGlfwGL3_NewFrame();

    {
      static float f = 0.0f;
      ImGui::Text("Debug Window!");
      ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
      ImGui::ColorEdit3("clear color", (float*)&clear_color);
      if (ImGui::Button("Test Window")) show_test_window ^= 1;
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    }

    // 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
    if (show_test_window) {
      ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_FirstUseEver);
      ImGui::ShowTestWindow(&show_test_window);
    }
	
    if(show_calibrator_GUI) {
      ImGui::SetNextWindowPos(ImVec2(650,20), ImGuiCond_FirstUseEver);
      calibrator.showGUI(&show_calibrator_GUI);
    }

    // Rendering
    int display_w, display_h;
    glfwGetFramebufferSize(window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui::Render();
    glfwSwapBuffers(window);

    //    ros::spinOnce();
  }

  // Cleanup
  ImGui_ImplGlfwGL3_Shutdown();
  glfwTerminate();
  
  return 0;
}
