#include <srrg_l2c_gui/trajectory_viewer.h>
#include <srrg_l2c_utils/file_reader.h>
#include <srrg_l2c_utils/trajectory_analyzer.h>

#include <srrg_system_utils/system_utils.h>

using namespace srrg_l2c;

const char* banner [] = {
  "trajectory_viewer_app: from nw to valhalla",
  "",
  "usage: trajectory_viewer_app file.txt",
  "-h     <flag>           this help",
  0
};


int main(int argc, char** argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  int c = 1;
  std::string file = "";
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else 
      file = argv[c];
    c++;
  }

  DatasetPtr dataset = DatasetPtr(new Dataset());
  
  FileReader filereader;
  filereader.setFile(file);
  if(!filereader.compute12(*dataset))
    std::runtime_error("[Main]: Error while processing the dataset");  

  TrajectoryAnalyzer trajectory_analyzer;
  trajectory_analyzer.compute(*dataset);
  
  std::cerr << "dataset size: " << dataset->size() << std::endl;
  
  QApplication app(argc, argv);
  TrajectoryViewer viewer;
  viewer.setDataset(dataset);

  viewer.show();
  
  return app.exec();
  
}
